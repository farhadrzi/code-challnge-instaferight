<?php

namespace Tests\Unit;

use App\Traits\CentsToUSDTrait;
use App\Traits\ShipmentPriceTrait;
use PHPUnit\Framework\TestCase;

class CalculateShipmentCostTest extends TestCase
{
    use ShipmentPriceTrait,CentsToUSDTrait;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_example()
    {
        $shipmentCost = $this->centToUSD($this->calculateShipmentPrice(340579));
        $this->assertEquals(0.8108685,$shipmentCost);
    }
}
