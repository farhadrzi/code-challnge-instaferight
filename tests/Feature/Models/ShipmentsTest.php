<?php

namespace Tests\Feature\Models;

use App\Models\Carriers;
use App\Models\Companies;
use App\Models\Routes;
use App\Models\Shipments;
use App\Models\Stops;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class ShipmentsTest extends TestCase
{
    private Carriers $carrier;
    private Companies $compnay;
    private Stops $stop;
    private Shipments $shipment;

    public function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate:refresh');
        $faker = Factory::create();
        $carrier =Carriers::firstOrCreate([
            'name'=>$faker->name,
            'email'=>$faker->email,
        ]);
        $compnay = Companies::firstOrCreate([
            'name'=>$faker->name,
            'email'=>$faker->email,
        ]);
        $this->stop = Stops::firstOrCreate([
            'postalcode'=>$faker->postcode,
            'city'=>$faker->city,
            'country'=>'DE',
        ]);
        $this->shipment = Shipments::firstOrCreate([
            'company_id'=>$compnay->id,
            'carrier_id'=>$carrier->id,
            'distance'=>$faker->randomDigitNotZero(),
            'time'=>$faker->randomDigitNotZero(),
            'cost'=>$faker->randomDigitNotZero(),
        ]);
        Routes::firstOrCreate([
            'shipment_id'=>$this->shipment->id,
            'stop_id'=>$this->stop->id,
        ]);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $params = [
            "filter" => [
                "stop"=>[
                    ['postalcode'=>$this->stop->postalcode]
                ]
            ]
        ];
        $response = $this->post('/api/shipment/list',$params, ['Accept' => 'application/json']);

        $response->assertStatus(200)->assertJson(["message"=>"success"]);
    }
}
