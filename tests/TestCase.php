<?php

namespace Tests;

use App\Models\Carriers;
use App\Models\Companies;
use App\Models\Routes;
use App\Models\Shipments;
use App\Models\Stops;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Support\Facades\Artisan;
use Exception;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;


    public function setUp()
    : void {
        parent::setUp();
    }
}
