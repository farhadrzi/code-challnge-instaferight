<?php

namespace App\Repositories;

use App\Interfaces\CarrierRepositoryInterface;
use App\Models\Carriers;
use Exception;

class CarriersRepository implements CarrierRepositoryInterface
{

    public function insertOrFind(array $data)
    {
        try {
            return Carriers::firstOrCreate($data);
        }catch (Exception $exception){
            throw new Exception('Error While Insert Data');
        }
    }

    public function findWithEmail($carrierEmail):Carriers|null
    {
        return Carriers::where('email',$carrierEmail)->first();
    }

    public function findWithName($carrierName):Carriers|null
    {
        return Carriers::where('name',$carrierName)->first();
    }
}
