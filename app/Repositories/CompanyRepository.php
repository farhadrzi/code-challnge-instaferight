<?php


namespace App\Repositories;


use App\Interfaces\CompanyRepositoryInterface;
use App\Models\Companies;
use Exception;

class CompanyRepository implements CompanyRepositoryInterface
{
    /**
     * @param array $data
     * @return Companies|Exception
     * @throws Exception
     */
    public function insertOrFind(array $data):Companies|Exception
    {
        try {
            return Companies::firstOrCreate($data);
        }catch (Exception $exception){
            throw new Exception('Error While Insert Data');
        }
    }

    /**
     * @param $companyEmail
     * @return Companies|null
     */
    public function findWithEmail($companyEmail):Companies|null
    {
        return Companies::where('email',$companyEmail)->first();
    }

    /**
     * @param $companyName
     * @return Companies|null
     */
    public function findWithName($companyName):Companies|null
    {
        return Companies::where('name',$companyName)->first();
    }
}
