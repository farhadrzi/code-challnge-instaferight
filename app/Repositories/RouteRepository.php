<?php


namespace App\Repositories;


use App\Interfaces\RouteRepositoryInterface;
use App\Models\Routes;
use Exception;


class RouteRepository implements RouteRepositoryInterface
{
    /**
     * @param array $data
     * @return Routes|Exception
     * @throws Exception
     */
    public function insert(array $data):Routes|Exception{
        try {
            return Routes::create($data);
        }catch (Exception $exception){
            throw new Exception('Error While Insert Data');
        }
    }

    /**
     * @param array $data
     * @return Routes|Exception
     * @throws Exception
     */
    public function insertOrFind(array $data):Routes|Exception{
        try {
            return Routes::firstOrCreate($data);
        }catch (Exception $exception){
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @param $shipmentId
     * @return array
     */
    public function findWhitShipmentId($shipmentId):array{
        return Routes::where('shipment_id',$shipmentId)->get();
    }

    /**
     * @param $stopId
     * @return array
     */
    public function findWhitStopId($stopId):array{
        return Routes::where('stop_id',$stopId)->get();
    }

    public function attachStops(array $data)
    {
        // TODO: Implement attachStops() method.
    }
}
