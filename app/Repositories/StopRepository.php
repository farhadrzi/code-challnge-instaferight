<?php


namespace App\Repositories;


use App\Interfaces\StopRepositoryInterface;
use App\Models\Stops;
use Exception;

class StopRepository implements StopRepositoryInterface
{
    public function insertOrFind(array $data): Stops
    {
        return  Stops::firstOrCreate($data);
    }

    public function findWithId($stopId):Stops|Exception{
        try {
            return Stops::where('id',$stopId)->first();
        }catch (Exception $exception){
            throw new Exception('Error While Insert Data');
        }
    }

    public function findWithPostalCode($postalCode):array{
        return Stops::where('postalcode',$postalCode)->get();
    }

    public function findWithCity($city):array{
         return Stops::where('city',$city)->get();
    }

    public function findWithCountry($country):array{
        return Stops::where('country',$country)->get();
    }
}
