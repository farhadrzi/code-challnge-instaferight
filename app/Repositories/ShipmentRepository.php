<?php


namespace App\Repositories;


use App\Http\Requests\StoreShipmentsRequest;
use App\Interfaces\ShipmentRepositoryInterface;
use App\Models\Shipments;
use App\Models\Stops;
use App\Traits\CentsToUSDTrait;
use App\Traits\ShipmentPriceTrait;
use Exception;

class ShipmentRepository implements ShipmentRepositoryInterface
{
    use ShipmentPriceTrait,CentsToUSDTrait;

    /**
     * @param array $data
     * @return Shipments|Exception
     * @throws Exception
     */
    public function insert(array $data):Shipments|Exception{
        try {
            $data['cost']=$this->centToUSD($this->calculateShipmentPrice($data['distance']));
            return Shipments::create($data);
        }catch (Exception $exception){
            throw new Exception('Error While Insert Shipment');
        }
    }

    /**
     * @param array $data
     * @return Shipments
     */
    public function insertOrFind(array $data):Shipments{
        $data['cost']=$this->centToUSD($this->calculateShipmentPrice($data['distance']));
        return Shipments::firstOrCreate($data);
    }

    /**
     * @param $shipmentId
     * @return Shipments|Exception
     * @throws Exception
     */
    public function findWithId($shipmentId):Shipments|Exception{
        $shipment = Shipments::where('id',$shipmentId)->first();
        if($shipment){
            return $shipment;
        }else{
            throw new Exception('Shipment Not Found');
        }
    }

    /**
     * @param $companyId
     * @return array
     */
    public function findWithCompanyId($companyId):array{
        return Shipments::where('company_id',$companyId)->get();
    }

    /**
     * @param $carrierId
     * @return array
     */
    public function findWithCarrierId($carrierId):array{
        return Shipments::where('carrier_id',$carrierId)->get();
    }

    /**
     * @param Shipments $shipments
     * @param $stopsId
     */
    public function attachStops(Shipments $shipments, $stopsId)
    {
        //todo:Attach Data
    }
}
