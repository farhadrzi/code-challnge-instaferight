<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Routes extends Model
{
    use HasFactory;
    protected $fillable = ['shipment_id','stop_id'];

    /**
     * The attributes that should be hidden
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'id'
    ];
}
