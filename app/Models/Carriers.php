<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Carriers extends Model
{
    use HasFactory;

    protected $fillable = ['name','email'];


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'id',
    ];

    /**
     * Reformat For The Response
     * @return array
     */
    public function format(){
        return[
            "name"=> $this->name,
            "email"=> $this->email
        ];
    }
}
