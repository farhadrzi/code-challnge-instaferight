<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Stops extends Model
{
    use HasFactory;

    protected $fillable = ['id','postalcode','city','country'];


    /**
     * The stop that belong to the Shipments
     *
     * @return BelongsToMany
     */
    public function shipment(): BelongsToMany
    {
        return $this->belongsToMany(Shipments::class, 'routes', 'stop_id', 'shipment_id');
    }

    /**
     * Reformat For The Response
     * @return array
     */
    public function format(){
        return[
            "stop_id"=> $this->id,
            "postalcode"=> $this->postalcode,
            "city"=> $this->city,
            "country"=> $this->country,
        ];
    }
}
