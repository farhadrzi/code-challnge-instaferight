<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    use HasFactory;

    protected $fillable = ['name','email'];


    /**
     * Reformat The Response
     * @return array
     */
    public function format(){
        return[
            "name"=> $this->name,
            "email"=> $this->email
        ];
    }

}
