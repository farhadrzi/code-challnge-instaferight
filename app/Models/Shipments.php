<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Shipments extends Model
{
    use HasFactory;
    protected $fillable = ['id','distance','time','company_id',
        'carrier_id',
        'cost'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [

        ];

    /**
     * Get the carrier that owns the Shipments
     *
     * @return BelongsTo
     */
    public function carrier(): BelongsTo
    {
        return $this->belongsTo(Carriers::class, 'carrier_id', 'id');
    }

    /**
     * Get the company that owns the Shipments
     *
     * @return BelongsTo
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Companies::class, 'company_id', 'id');
    }

    /**
     * The stops that belong to the Shipments
     *
     * @return BelongsToMany
     */
    public function stops(): BelongsToMany
    {
        return $this->belongsToMany(Stops::class, 'routes', 'shipment_id', 'stop_id');
    }

    /**
     * Reformat The Response
     * @return array
     */
    public function format(){
        return[
            "id"=> $this->id,
            "distance"=> $this->distance,
            "time"=> $this->time,
        ];
    }
    /**
     * Reformat The Response
     * @return array
     */
    public function formatAll(){
        $route= array();
        foreach ($this->stops as $stop){
            $route[]=$stop->format();
        }
        return[
            "id"=> $this->id,
            "distance"=> $this->distance,
            "time"=> $this->time,
            "cost"=> $this->cost.'$',
            "company"=> $this->company->format(),
            "carrier"=> $this->carrier->format(),
            "route"=>$route ,
        ];
    }
    /**
     * Reformat The Response
     * @return array
     */
    public function formatWithCost(){
        return[
            "id"=> $this->id,
            "distance"=> $this->distance,
            "time"=> $this->time,
            "cost"=> $this->cost,
        ];
    }
}
