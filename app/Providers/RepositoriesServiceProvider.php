<?php

namespace App\Providers;

use App\Interfaces\CarrierRepositoryInterface;
use App\Interfaces\CompanyRepositoryInterface;
use App\Interfaces\RouteRepositoryInterface;
use App\Interfaces\ShipmentRepositoryInterface;
use App\Interfaces\StopRepositoryInterface;
use App\Repositories\CarriersRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\RouteRepository;
use App\Repositories\ShipmentRepository;
use App\Repositories\StopRepository;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(CarrierRepositoryInterface::class,CarriersRepository::class);
        $this->app->bind(CompanyRepositoryInterface::class,CompanyRepository::class);
        $this->app->bind(StopRepositoryInterface::class,StopRepository::class);
        $this->app->bind(ShipmentRepositoryInterface::class,ShipmentRepository::class);
        $this->app->bind(RouteRepositoryInterface::class,RouteRepository::class);
    }
}
