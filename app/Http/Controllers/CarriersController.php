<?php

namespace App\Http\Controllers;

use App\Models\Carriers;
use App\Http\Requests\StoreCarriersRequest;
use App\Http\Requests\UpdateCarriersRequest;

class CarriersController extends AppBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCarriersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCarriersRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Carriers  $carriers
     * @return \Illuminate\Http\Response
     */
    public function show(Carriers $carriers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Carriers  $carriers
     * @return \Illuminate\Http\Response
     */
    public function edit(Carriers $carriers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCarriersRequest  $request
     * @param  \App\Models\Carriers  $carriers
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCarriersRequest $request, Carriers $carriers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Carriers  $carriers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Carriers $carriers)
    {
        //
    }
}
