<?php

namespace App\Http\Controllers;

use App\Models\Stops;
use App\Http\Requests\StoreStopsRequest;
use App\Http\Requests\UpdateStopsRequest;

class StopsController extends AppBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreStopsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStopsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Stops  $stops
     * @return \Illuminate\Http\Response
     */
    public function show(Stops $stops)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Stops  $stops
     * @return \Illuminate\Http\Response
     */
    public function edit(Stops $stops)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateStopsRequest  $request
     * @param  \App\Models\Stops  $stops
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStopsRequest $request, Stops $stops)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Stops  $stops
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stops $stops)
    {
        //
    }
}
