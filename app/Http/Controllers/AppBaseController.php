<?php


namespace App\Http\Controllers;


use App\Helpers\ResponseHelper;

class AppBaseController extends Controller
{
    public ResponseHelper $response;
    public function __construct()
    {
        $this->response =  new ResponseHelper();
    }
}
