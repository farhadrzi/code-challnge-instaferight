<?php

namespace App\Http\Controllers;

use App\Models\Shipments;
use App\Http\Requests\StoreShipmentsRequest;
use App\Http\Requests\UpdateShipmentsRequest;
use App\Services\ShipmentManagementService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ShipmentsController extends AppBaseController
{
    /**
     * @var ShipmentManagementService
     */
    private ShipmentManagementService $shipmentManagementService;

    public function __construct(ShipmentManagementService $shipmentManagementService)
    {
        parent::__construct();
        $this->shipmentManagementService = $shipmentManagementService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        try {
            if($request->input('filter')==null){
                throw new Exception('Filter Must Be On Response');
            }
            $shipment = $this->shipmentManagementService->getAllShipment($request->filter);
            return $this->response->success($shipment->values(),'success',200);
        }catch (Exception $exception){
            return  $this->response->error($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreShipmentsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreShipmentsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shipments  $shipments
     * @return \Illuminate\Http\Response
     */
    public function show(Shipments $shipments)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Shipments  $shipments
     * @return \Illuminate\Http\Response
     */
    public function edit(Shipments $shipments)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateShipmentsRequest  $request
     * @param  \App\Models\Shipments  $shipments
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateShipmentsRequest $request, Shipments $shipments)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shipments  $shipments
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shipments $shipments)
    {
        //
    }
}
