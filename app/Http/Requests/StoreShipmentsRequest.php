<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreShipmentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'distance'          => 'required',
            'time'              => 'required',
            'company_email'     => 'required|string,email',
            'carrier_email'     => 'required|string,email',
            'route'          =>  'present|array',
            'route.*.postcode'      => 'required|numeric',
            'route.*.city'      => 'required|string',
            'route.*.country'      => ['required', Rule::in(['DE'])],
        ];
    }
}
