<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class downloadShipments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'download:shipments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download Shipments JSON File';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $contents = file_get_contents('https://raw.githubusercontent.com/igor822/instafreight-code-challenge/master/shipments.json');
        Storage::disk('public')->put('shipments.json', $contents);
        return 1;
    }
}
