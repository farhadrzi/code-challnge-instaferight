<?php


namespace App\Services;


use App\Interfaces\ShipmentRepositoryInterface;
use App\Models\Shipments;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use function Termwind\ValueObjects\isEmpty;

class ShipmentManagementService
{
    /**
     * @var ShipmentRepositoryInterface
     */
    private ShipmentRepositoryInterface $shipmentRepositoryInterface;
    /**
     * @var CarrierManagementService
     */
    private CarrierManagementService $carrierManagementService;
    /**
     * @var CompanyManagementService
     */
    private CompanyManagementService $companyManagementService;
    /**
     * @var StopManagementService
     */
    private StopManagementService $stopManagementService;

    public function __construct(ShipmentRepositoryInterface $shipmentRepositoryInterface,
                                CarrierManagementService $carrierManagementService,
                                CompanyManagementService $companyManagementService,
                                StopManagementService $stopManagementService)
    {
        $this->shipmentRepositoryInterface = $shipmentRepositoryInterface;
        $this->carrierManagementService = $carrierManagementService;
        $this->companyManagementService = $companyManagementService;
        $this->stopManagementService = $stopManagementService;
    }
    public function getShipmentWithId($shipmentId){
        try {
            return $this->shipmentRepositoryInterface->findWithId($shipmentId);
        }catch (Exception $exception){
            throw new Exception('Error While Get Shipment Data');
        }
    }
    public function insertShipment(array $data){
        try {
            return $this->shipmentRepositoryInterface->insertOrFind($data);
        }catch (Exception $exception){
            throw new Exception('Error While Insert Shipment');
        }
    }
    public function attachStopToShipment($shipment,$stopId){
        try {
            $this->shipmentRepositoryInterface->attachStops($shipment,$stopId);
        }catch (Exception $exception){
            throw new Exception('Error While Insert Route');
        }
    }

    public function getAllShipment(array $filter){
        try {
            $shipments = Shipments::with('carrier','company','stops')->get();
            if($filter!=[]){
                $shipments = $this->carrierManagementService->filterBaseOfCarrier($shipments,$filter);
                $shipments = $this->companyManagementService->filterBaseOfCompany($shipments,$filter);
                $shipments = $this->stopManagementService->filterBaseOfStop($shipments,$filter);
            }
            return $shipments->map->formatAll();
        }catch (Exception $exception){
            throw new Exception($exception->getMessage());
        }
    }
}
