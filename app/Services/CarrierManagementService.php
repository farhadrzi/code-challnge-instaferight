<?php

namespace App\Services;

use App\Interfaces\CarrierRepositoryInterface;
use App\Models\Carriers;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class CarrierManagementService{
    /**
     * @var CarrierRepositoryInterface
     */
    private CarrierRepositoryInterface $carrierRepositoryInterface;

    public function __construct(CarrierRepositoryInterface $carrierRepositoryInterface)
    {
        $this->carrierRepositoryInterface = $carrierRepositoryInterface;
    }

    /**
     * @param $carrierEmail
     * @return mixed
     * @throws Exception
     */
    public function getCarrierWithEmail($carrierEmail){
        $carrier = $this->carrierRepositoryInterface->findWithEmail($carrierEmail);
        if($carrier){
            return $carrier;
        }else{
            throw new Exception('Carrier Not Found');
        }
    }

    /**
     * @param $carrierName
     * @return mixed
     * @throws Exception
     */
    public function getCarrierWithName($carrierName){
        $carrier = $this->carrierRepositoryInterface->findWithEmail($carrierName);
        if($carrier){
            return $carrier;
        }else{
            throw new Exception('Carrier Not Found');
        }
    }

    /**
     * @param array $data
     * @return Carriers
     */
    public function insertOrFindCarrier(array $data):Carriers{
        return $this->carrierRepositoryInterface->insertOrFind($data);
    }

    public function filterCarriersArray(array $carriers):array{
        try {
            $carrierEmails=array();
            $carrierNames=array();
            foreach ($carriers as $carrier){
                if(array_key_exists('name',$carrier)){
                    $carrierNames[]=$carrier['name'];
                }
                if(array_key_exists('email',$carrier)){
                    $carrierEmails[]=$carrier['email'];
                }
            }
            return['carrierNames'=>$carrierNames,'carrierEmails'=>$carrierEmails];
        }catch (Exception $exception){
            throw new Exception($exception->getMessage());
        }
    }
    public function filterModelBaseOfCarrierEmail(Collection $model,array $email){
        try {
            return $model->whereIn('carrier.email',$email);
        }catch (Exception $exception){
            throw new Exception($exception->getMessage());
        }
    }

    public function filterModelBaseOfCarrierName(Collection $model,array $name){
        try {
            return $model->whereIn('carrier.name',$name);
        }catch (Exception $exception){
            throw new Exception($exception->getMessage());
        }
    }

    public function filterBaseOfCarrier(Collection $model,array $filter):Collection{
        try {
            $filteredModel = $model;
            if(array_key_exists('carrier',$filter)){
                $carrierData = $this->filterCarriersArray($filter['carrier']);
                if($carrierData['carrierEmails']!=[]){
                    $filteredModel = $this->filterModelBaseOfCarrierEmail($filteredModel,$carrierData['carrierEmails']);
                }
                if($carrierData['carrierNames']!=[]){
                    $filteredModel = $this->filterModelBaseOfCarrierName($filteredModel,$carrierData['carrierNames']);
                }
            }
            return $filteredModel;
        }catch (Exception $exception){
            throw new Exception($exception->getMessage());
        }
    }
}
