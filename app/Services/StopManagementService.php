<?php


namespace App\Services;


use App\Interfaces\StopRepositoryInterface;
use App\Models\Shipments;
use App\Models\Stops;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use function GuzzleHttp\Promise\all;

class StopManagementService
{
    /**
     * @var StopRepositoryInterface
     */
    private StopRepositoryInterface $stopRepositoryInterface;

    /**
     * StopManagementService constructor.
     * @param StopRepositoryInterface $stopRepositoryInterface
     */
    public function __construct(StopRepositoryInterface $stopRepositoryInterface)
    {
        $this->stopRepositoryInterface = $stopRepositoryInterface;
    }

    /**
     * @param array $data
     * @return Stops
     */
    public function insertOrFind(array $data):Stops{
        return $this->stopRepositoryInterface->insertOrFind($data);

    }

    /**
     * @param $stopId
     * @return Stops|Exception
     * @throws Exception
     */
    public function getStopWithId($stopId):Stops|Exception{
        $stop = $this->stopRepositoryInterface->findWithId($stopId);
        if($stop){
            return $stop->format();
        }else{
            throw new Exception('Stop Not Found');
        }
    }

    /**
     * @param $postalCode
     * @return Stops
     */
    public function getStopWithPostalCode($postalCode):Stops{
        $stops = $this->stopRepositoryInterface->findWithPostalCode($postalCode);
        return $stops->format();
    }

    /**
     * @param $city
     * @return Stops
     */
    public function getStopWithCity($city):Stops{
        $stops = $this->stopRepositoryInterface->findWithCity($city);
        return $stops->format();
    }

    /**
     * @param $county
     * @return Stops
     */
    public function getStopWithCounty($county):Stops{
        $stops = $this->stopRepositoryInterface->findWithCountry($county);
        return $stops->format();
    }
    public function filterStopsArray(array $stops):array{
        $stopPostalCode=array();
        $stopCity=array();
        $stopCountry=array();
        foreach ($stops as $stop){
            if(array_key_exists('postalcode',$stop)){
                $stopPostalCode[]=$stop['postalcode'];
            }

            if(array_key_exists('city',$stop)){
                $stopCity[]=$stop['city'];
            }

            if(array_key_exists('country',$stop)){
                $stopCountry[]=$stop['country'];
            }
        }
        return['stopPostalCode'=>$stopPostalCode,'stopCity'=>$stopCity,'stopCountry'=>$stopCountry];
    }
    public function checkStopsDataArray($value,$array):bool{
        try {
            foreach ($array as $stopValue){
                if($value==$stopValue){
                    return true;
                }
            }
            return false;
        }catch (Exception $exception){
            throw new Exception($exception->getMessage());
        }
    }
    public function filterModelBaseOfStopPostalCode(Collection $model,array $postalCode){
        try {
            $filteredModel =collect();
            $isFound = false;
            foreach ($model as $shipment){
                foreach ($shipment->stops as $stop){
                    if($this->checkStopsDataArray($stop->postalcode,$postalCode)){
                        $isFound = true;
                        $filteredModel->push($shipment);
                    }
                }
            }
            if(!$isFound){
                $filteredModel = $model;
            }
            return Collection::make($filteredModel->values());
        }catch (Exception $exception){
            throw new Exception($exception->getMessage());
        }
    }

    public function filterModelBaseOfStopCity(Collection $model,array $city){
        try {
            $filteredModel =collect();
            $isFound = false;
            foreach ($model as $shipment){
                foreach ($shipment->stops as $stop){
                    if($this->checkStopsDataArray($stop->city,$city)){
                        $isFound = true;
                        $filteredModel->push($shipment);
                    }
                }
            }
            if(!$isFound){
                $filteredModel = $model;
            }
            return Collection::make($filteredModel->values());
        }catch (Exception $exception){
            throw new Exception($exception->getMessage());
        }
    }

    public function filterModelBaseOfStopCountry(Collection $model,array $country){
        try {
            $filteredModel =collect();
            $isFound = false;
            foreach ($model as $shipment){
                foreach ($shipment->stops as $stop){
                    if($this->checkStopsDataArray($stop->country,$country)){
                        $isFound = true;
                        $filteredModel->push($shipment);
                    }
                }
            }
            if(!$isFound){
                $filteredModel = $model;
            }
            return Collection::make($filteredModel->values());
        }catch (Exception $exception){
            throw new Exception($exception->getMessage());
        }
    }

    public function filterBaseOfStop(Collection $model,array $filter):Collection|Exception{
        try {
            $filteredModel = $model;
            if(array_key_exists('stop',$filter)){
                $stopData = $this->filterStopsArray($filter['stop']);
                if($stopData['stopPostalCode']!=[]){
                    $filteredModel = $this->filterModelBaseOfStopPostalCode($filteredModel,$stopData['stopPostalCode']);
                }
                if($stopData['stopCity']!=[]){
                    $filteredModel = $this->filterModelBaseOfStopCity($filteredModel,$stopData['stopCity']);
                }
                if($stopData['stopCountry']!=[]){
                    $filteredModel = $this->filterModelBaseOfStopCountry($filteredModel,$stopData['stopCountry']);
                }
            }
            return $filteredModel;
        }catch (Exception $exception){
            throw new Exception($exception->getMessage());
        }
    }
}
