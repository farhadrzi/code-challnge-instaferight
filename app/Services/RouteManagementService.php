<?php


namespace App\Services;


use App\Interfaces\RouteRepositoryInterface;

class RouteManagementService
{
    /**
     * @var RouteRepositoryInterface
     */
    private RouteRepositoryInterface $routeRepositoryInterface;

    public function __construct(RouteRepositoryInterface $routeRepositoryInterface)
    {
        $this->routeRepositoryInterface = $routeRepositoryInterface;
    }

    public function insert(array $data){
        try {
            $this->routeRepositoryInterface->insert($data);
        }catch (\Exception $exception){
            throw new \Exception('Error While Insert Data');
        }
    }
    public function insertOrFind(array $data){
        try {
            $this->routeRepositoryInterface->insertOrFind($data);
        }catch (\Exception $exception){
            throw new \Exception($exception->getMessage());
        }
    }
}
