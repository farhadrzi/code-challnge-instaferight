<?php


namespace App\Services;


use App\Interfaces\CompanyRepositoryInterface;
use App\Models\Companies;
use Illuminate\Database\Eloquent\Collection;

class CompanyManagementService
{
    /**
     * @var CompanyRepositoryInterface
     */
    private CompanyRepositoryInterface $companyRepositoryInterface;

    public function __construct(CompanyRepositoryInterface $companyRepositoryInterface)
    {
        $this->companyRepositoryInterface = $companyRepositoryInterface;
    }

    public function insertOrFind(array $data):Companies{
        return $this->companyRepositoryInterface->insertOrFind($data);
    }

    public function getCompanyWithEmail($companyEmail){
        $company = $this->companyRepositoryInterface->findWithEmail($companyEmail);
        if($company){
            return $company;
        }else{
            throw new \Exception('Company Not Found');
        }
    }

    public function getCompanyWithName($companyName){
        $company = $this->companyRepositoryInterface->findWithName($companyName);
        if($company){
            return $company;
        }else{
            throw new \Exception('Company Not Found');
        }
    }
    public function filterCompaniesArray(array $companies):array{
        try {
            $companyEmails=array();
            $companyNames=array();
            foreach ($companies as $company){
                if(array_key_exists('email',$company)){
                    $companyEmails[]=$company['email'];
                }
                if(array_key_exists('name',$company)){
                    $companyNames[]=$company['name'];
                }
            }
            return['companyNames'=>$companyNames,'companyEmails'=>$companyEmails];
        }catch (\Exception $exception){
            throw new \Exception($exception->getMessage());
        }
    }

    public function filterModelBaseOfCompanyEmail(Collection $model,array $email){
        try {
            return $model->whereIn('company.email',$email);
        }catch (\Exception $exception){
            throw new \Exception($exception->getMessage());
        }
    }

    public function filterModelBaseOfCompanyName(Collection $model,array $name){
        try {
            return $model->whereIn('company.name',$name);
        }catch (\Exception $exception){
            throw new \Exception($exception->getMessage());
        }
    }

    public function filterBaseOfCompany(Collection $model,array $filter):Collection{
        try {
            $filteredModel = $model;
            if(array_key_exists('company',$filter)){
                $companyData = $this->filterCompaniesArray($filter['company']);
                if($companyData['companyEmails']!=[]){
                    $filteredModel = $this->filterModelBaseOfCompanyEmail($filteredModel,$companyData['companyEmails']);
                }
                if($companyData['companyNames']!=[]){
                    $filteredModel = $this->filterModelBaseOfCompanyName($filteredModel,$companyData['companyNames']);
                }
            }
            return  $filteredModel;
        }catch (\Exception $exception){
            throw new \Exception($exception->getMessage());
        }
    }
}
