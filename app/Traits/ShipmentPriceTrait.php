<?php


namespace App\Traits;


trait ShipmentPriceTrait
{
    /**
     * calculate Shipment Price
     * @param $distance
     * @return float|int
     */
    public function calculateShipmentPrice($distance):int|float{
        $totalPrice=0;
        $tempDistance = $distance;
        while ($tempDistance>0){
            if($tempDistance>300000){
                $difference = $tempDistance-300000;
                $tempDistance = $tempDistance-$difference;
                $totalPrice = $totalPrice+($difference*0.00015);
            }else if($tempDistance>200000){
                $difference = $tempDistance-200000;
                $tempDistance = $tempDistance-$difference;
                $totalPrice = $totalPrice+($difference*0.00020);
            }else if($tempDistance>100000){
                $difference = $tempDistance-100000;
                $tempDistance = $tempDistance-$difference;
                $totalPrice = $totalPrice+($difference*0.00025);
            }else{
                $totalPrice = $totalPrice+($tempDistance*0.00030);
                $tempDistance=0;
            }
        }
        return $totalPrice;
    }
}
