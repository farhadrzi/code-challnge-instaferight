<?php


namespace App\Traits;


use Illuminate\Support\Facades\Storage;

trait FileTraits
{
    public function downloadFile($url){
        try {
            $contents = file_get_contents($url);
            Storage::disk('public')->put('shipments.json', $contents);
        }catch (\Exception $exception){
            throw new \Exception('Error While Download File');
        }
    }

    public function openFile($fileName){
        return Storage::disk('public')->get($fileName);
    }
}
