<?php


namespace App\Traits;


trait CentsToUSDTrait
{
    /**
     *
     * Convert Cents To Dollar
     * @param $amount
     * @return float|int
     */
    public function centToUSD($amount):int|float{
        return $amount/100;
    }
}
