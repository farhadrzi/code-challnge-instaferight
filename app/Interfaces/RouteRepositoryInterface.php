<?php

namespace App\Interfaces;

use App\Models\Routes;
use Exception;

interface RouteRepositoryInterface
{
    public function insert(array $data): Routes|Exception;

    public function insertOrFind(array $data): Routes|Exception;

    public function attachStops(array $data);

    public function findWhitShipmentId($shipmentId): array;

    public function findWhitStopId($stopId): array;
}
