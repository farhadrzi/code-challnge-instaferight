<?php
namespace App\Interfaces;

interface CarrierRepositoryInterface {
    public function insertOrFind(array $data);
    public function findWithEmail($carrierEmail);
    public function findWithName($carrierName);
}
