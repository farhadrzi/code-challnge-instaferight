<?php

namespace App\Interfaces;

use App\Models\Shipments;
use App\Models\Stops;
use Exception;

interface ShipmentRepositoryInterface
{
    /**
     * @param array $data
     * @return Shipments|Exception
     * @throws Exception
     */
    public function insert(array $data): Shipments|Exception;

    /**
     * @param array $data
     * @return Shipments
     */
    public function insertOrFind(array $data): Shipments;

    /**
     * @param $shipmentId
     * @return Shipments|Exception
     * @throws Exception
     */
    public function findWithId($shipmentId): Shipments|Exception;

    /**
     * @param $companyId
     * @return array
     */
    public function findWithCompanyId($companyId): array;

    /**
     * @param $carrierId
     * @return array
     */
    public function findWithCarrierId($carrierId): array;

    /**
     * @param Shipments $shipments
     * @param $stopsId
     * @return mixed
     */
    public function attachStops(Shipments $shipments,$stopsId);

}
