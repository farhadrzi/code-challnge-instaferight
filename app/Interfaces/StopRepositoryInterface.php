<?php

namespace App\Interfaces;

use App\Models\Stops;
use Exception;

interface StopRepositoryInterface
{
    public function insertOrFind(array $data): Stops;

    public function findWithId($stopId): Stops|Exception;

    public function findWithPostalCode($postalCode): array;

    public function findWithCity($city): array;

    public function findWithCountry($country): array;
}
