<?php

namespace App\Interfaces;

interface CompanyRepositoryInterface
{
    public function insertOrFind(array $data);

    public function findWithEmail($companyEmail);

    public function findWithName($companyName);
}
