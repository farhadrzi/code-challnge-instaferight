<?php

namespace Database\Seeders;

use App\Models\Stops;
use App\Services\StopManagementService;
use App\Traits\FileTraits;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\App;

class StopsSeeder extends Seeder
{
    use FileTraits;

    /**
     * @var StopManagementService
     */
    private StopManagementService $stopManagementService;

    public function __construct(StopManagementService $stopManagementService)
    {
        $this->stopManagementService = $stopManagementService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App::setLocale('de');
        $file = $this->openFile('shipments.json');
        $json = json_decode($file);
        foreach($json as $shipment){
            $stops = $shipment->route;
            foreach($stops as $stop){
                $this->stopManagementService->insertOrFind(['id' => $stop->stop_id,'postalcode'=>$stop->postcode,'city'=>$stop->city,'country'=>$stop->country]);
            }
        }
    }
}
