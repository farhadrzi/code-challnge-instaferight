<?php

namespace Database\Seeders;

use App\Services\CarrierManagementService;
use App\Traits\FileTraits;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class CarriersSeeder extends Seeder
{
    use FileTraits;

    /**
     * @var CarrierManagementService
     */
    private CarrierManagementService $carrierManagementService;

    public function __construct(CarrierManagementService $carrierManagementService)
    {
        $this->carrierManagementService = $carrierManagementService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App::setLocale('de');
        $file = $this->openFile('shipments.json');
        $json = json_decode($file);
        foreach($json as $shipment){
            try {
                $carrier = $shipment->carrier;
                $this->carrierManagementService->insertOrFindCarrier(['email' => $carrier->email,'name'=>$carrier->name]);
            }catch (\Exception $exception){
                continue;
            }
        }
    }
}
