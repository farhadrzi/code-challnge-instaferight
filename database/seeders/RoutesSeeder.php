<?php

namespace Database\Seeders;

use App\Models\Routes;
use App\Services\RouteManagementService;
use App\Services\ShipmentManagementService;
use App\Traits\FileTraits;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;

class RoutesSeeder extends Seeder
{
    use FileTraits;



    /**
     * @var RouteManagementService
     */
    private RouteManagementService $routeManagementService;

    public function __construct(RouteManagementService $routeManagementService)
    {

        $this->routeManagementService = $routeManagementService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App::setLocale('de');
        $file = $this->openFile('shipments.json');
        $json = json_decode($file);
        foreach($json as $shipment){
            $stops = $shipment->route;
            foreach ($stops as $stop){
                try {
                    $this->routeManagementService->insertOrFind(['shipment_id'=>$shipment->id,'stop_id'=>$stop->stop_id]);
                }catch (\Exception $exception){
                    error_log($exception->getMessage());
                }
            }
        }
    }
}
