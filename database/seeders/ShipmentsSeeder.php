<?php

namespace Database\Seeders;

use App\Models\Shipments;
use App\Services\CarrierManagementService;
use App\Services\CompanyManagementService;
use App\Services\ShipmentManagementService;
use App\Traits\FileTraits;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;

class ShipmentsSeeder extends Seeder
{
    use FileTraits;
    /**
     * @var ShipmentManagementService
     */
    private ShipmentManagementService $shipmentManagementService;
    /**
     * @var CarrierManagementService
     */
    private CarrierManagementService $carrierManagementService;
    /**
     * @var CompanyManagementService
     */
    private CompanyManagementService $companyManagementService;


    public function __construct(ShipmentManagementService $shipmentManagementService,
                                CarrierManagementService $carrierManagementService,
                                CompanyManagementService $companyManagementService)
    {
        $this->shipmentManagementService = $shipmentManagementService;
        $this->carrierManagementService = $carrierManagementService;
        $this->companyManagementService = $companyManagementService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App::setLocale('de');
        $file = $this->openFile('shipments.json');
        $json = json_decode($file);
        foreach ($json as $shipment) {
            $carrier = $this->carrierManagementService->getCarrierWithEmail($shipment->carrier->email);
            $company = $this->companyManagementService->getCompanyWithEmail($shipment->company->email);
            $this->shipmentManagementService->insertShipment(['id' => $shipment->id, 'distance' => $shipment->distance,
                'time' => $shipment->time,
                'company_id' => $company->id,
                'carrier_id' => $carrier->id]);
        }
    }
}
