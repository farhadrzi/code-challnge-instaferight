<?php

namespace Database\Seeders;

use App\Models\Companies;
use App\Services\CompanyManagementService;
use App\Traits\FileTraits;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\App;

class CompaniesSeeder extends Seeder
{
    use FileTraits;

    /**
     * @var CompanyManagementService
     */
    private CompanyManagementService $companyManagementService;

    public function __construct(CompanyManagementService $companyManagementService)
    {
        $this->companyManagementService = $companyManagementService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App::setLocale('de');
        $file = $this->openFile('shipments.json');
        $json = json_decode($file);
        foreach($json as $shipment){
            $company = $shipment->company;
            $this->companyManagementService->insertOrFind(['email' => $company->email,'name'=>$company->name]);
        }
    }
}
