<?php

namespace Database\Factories;

use App\Models\Carriers;
use App\Models\Companies;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Shipments>
 */
class ShipmentsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'distance' => fake()->randomDigitNotZero(),
            'time' => fake()->randomDigitNotZero(),
            'company_id' => Companies::inRandomOrder()->first()->id,
            'carrier_id' => Carriers::inRandomOrder()->first()->id,
            'cost' => $this->faker->randomDigitNotZero(),
        ];
    }
}
