<?php

namespace Database\Factories;

use App\Models\Shipments;
use App\Models\Stops;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Routes>
 */
class RoutesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'shipment_id'=>Shipments::inRandomOrder()->first()->id,
            'stop_id'=>Stops::inRandomOrder()->first()->id,
        ];
    }
}
