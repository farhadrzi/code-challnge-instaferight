<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## How To Build And Run Project

To run this project, you need Docker. Please make sure you installed Docker on your device.
After that, you can run this project with this command:
```shell script
docker-compose up -d --build
```
After you run this command application creates and runs automatically.
This application gets data from this URL:

[Json Shipment Data](https://raw.githubusercontent.com/igor822/instafreight-code-challenge/master/shipments.json) \
after downloading the Data application, automatically run the database seeder to fill a database with these data. But first 
application check if data exists in the database or not, and if not exist, it adds the data.
if you want to disable auto insert data to the database, you can comment these lines in *start-container* 
At the root of the project:
```shell script

/usr/bin/php8.1 /var/www/html/artisan download:shipments

/usr/bin/php8.1 /var/www/html/artisan db:seed

```


## Define Shipment API Structure
the JSON file download from GitLab return shipment data needed to add to the database in this API; there are some 
essential things:
- each shipment return distance as the meter.
- each shipment only have two stop first is the start point, and the second one is the destination.
- each Company may have multiple shipments. So need to detect each shipment for which is Company.
- each carrier may have multiple shipments. So need to notice each shipment for which is the carrier.
- each shipment has a route and two stops, so you can use pivot relation on laravel to optimize your query for finding stops.
This JSON has a unique German character, so your database charset must be UTF-8 to support this character.



## Shipment API List In Application

This API return list of all shipment, but there is some important things need to mention that:

- method of request Is POST.
- In the request's body, you must have JSON Object with key **filter**.
- if the key filter is empty, API returns all shipments data.
- if in JSONObject **filter** you put **carrier** JSON Array, you can filter shipment data with carriers' names or emails.
- if in JSONObject **filter** you put **Company** JSON Array, you can filter shipment data with a company name or email.
- if in JSONObject **filter** you put **stop** JSON Array, you can filter shipment data with stop postal code, city, or country.
- all these JSON Arrays Mentions in the top are optional, so you can filter with all of them or each you want.
- In the project's root, there was an API documentation base of open API structure. The file name is **APIDocumentation.json**.
also you can see the api document from this [URL](https://documenter.getpostman.com/view/11867545/UzXNVHyN).
- in the root of the project, there was a pdf with the name **DatabaseStruct.png** to show you the structure of the database.

### Gitlab CI/CD
In this project, there is automation for deploying the code on the server, so if you merge code with the branch master.
The deployment process starts automatically.

## Monitoring
this application connects to newRelic to monitor data and application status.

### Database Structure

<p align="center"><img src="https://www.dropbox.com/s/6necgojd888ywn2/DatabaseStruct.png?dl=1" width="720" height="812"></p>


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
